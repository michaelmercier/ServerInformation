// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerInformation.h"
#include "IronGate.h"

#include <fstream> 
#include <string>
#include <ctime>
 
using namespace std;

FString  path;

void UServerInformation::CountPlayer(int nbPlayer)
{
	string sPlayer = to_string(nbPlayer) + "/";
	FString fPlayer = sPlayer.c_str();

	//Get info inside the file.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);
	fInfo = fInfo + fPlayer;

	//Insert into file.
	FFileHelper::SaveStringToFile(fInfo, *path);
}

void UServerInformation::GameStatus(bool status)
{
	string sStatus = to_string(status) + "/";
	FString fStatus = sStatus.c_str();

	//Get info inside the file.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);
	fInfo = fInfo + fStatus;

	//Insert into file.
	FFileHelper::SaveStringToFile(fInfo, *path);
}

void UServerInformation::StartGame()
{
	//Create json file.
	CreateJsonFile();

	//Get time
	long int lTime = GetLocalTime();

	//Convert to FString
	string sTime = to_string(lTime) + "/";
	FString fTime = sTime.c_str();

	//Insert into file.
	FFileHelper::SaveStringToFile(fTime, *path);
}

void UServerInformation::StopGame()
{
	//Get time
	long int lTime = GetLocalTime();

	//Convert to FString
	string sTime = to_string(lTime) + "/";
	FString fTime = sTime.c_str();

	//Get info inside the file.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);
	fInfo = fInfo + fTime;

	//Insert into file.
	FFileHelper::SaveStringToFile(fInfo, *path);
}

//Create a Json file for every game.
void UServerInformation::CreateJsonFile()
{
	//Get the local time
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	//Unique name for the file.
	string sFileName = to_string(now.tm_year + 1900) + '-' + to_string(now.tm_mon + 1) + '-' + to_string(now.tm_mday) + '-' +
		to_string(now.tm_hour) + '-' + to_string(now.tm_min) + '-' + to_string(now.tm_sec) + ".json";

	//File Path
	FString fFileName = sFileName.c_str();
	//FString fPath = FPaths::EngineDir() + fFileName;  
	FString fPath = FPaths::GameDir() + fFileName;
	path = fPath;
}

long int UServerInformation::GetLocalTime()
{
	//The moment that the game started.
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	//Converting cTime to Long. 
	long int lTime = static_cast<long int> (t);

	return lTime;
}

void UServerInformation::GameId(int id)
{
	string sId = to_string(id) + "/";
	FString fId = sId.c_str();

	//Get info inside the file.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);
	fInfo = fInfo + fId;

	//Insert into file.
	FFileHelper::SaveStringToFile(fInfo, *path);
}

//Insert IP adress of this computer into file.
void UServerInformation::ReceiveIP(FString ip)
{
	FString fId = ip;

	//Get info inside the file.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);
	fInfo = fInfo + fId;

	//Insert into file.
	FFileHelper::SaveStringToFile(fInfo, *path);
}

//Connect & Send data to server.
void UServerInformation::UploadFile()
{
	// Read file and delimite.
	FString fInfo;
	FFileHelper::LoadFileToString(fInfo, *path);

	//Slip the line.
	FString Str = fInfo;
	TArray<FString> Parsed;
	Str.ParseIntoArray(Parsed, TEXT("/"), false);

	// Connection info.
	const FString & uploadUrl = "http://188.166.231.103/serverManager.php";
	FHttpModule * http = &FHttpModule::Get();
	TSharedRef<IHttpRequest> Request = http->CreateRequest();

	Request->SetURL(uploadUrl);
	Request->SetVerb(TEXT("POST"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json; charset=utf-8"));

	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
	JsonObject->SetStringField(TEXT("startGame"), Parsed[0]);
	JsonObject->SetStringField(TEXT("idGame"), Parsed[1]);
	JsonObject->SetNumberField(TEXT("nbPlayer"), FCString::Atoi(*Parsed[2]));
	JsonObject->SetNumberField(TEXT("gameStatus"), FCString::Atoi(*Parsed[3]));
	JsonObject->SetStringField(TEXT("stopGame"), Parsed[4]);

	FString OutputString;
	TSharedRef<TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), JsonWriter);

	Request->SetContentAsString(OutputString);

	UE_LOG(LogTemp, Warning, TEXT("File Loaded & is on process request"));
	Request->ProcessRequest();

	//Delete file
	string sConvert(TCHAR_TO_UTF8(*path));
	const char * file = sConvert.c_str();
	remove(file);
}



