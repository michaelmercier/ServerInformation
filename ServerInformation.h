// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameFramework/Actor.h"
#include "RandomStream.h"
#include "Http.h"
#include "Base64.h"
#include "ServerInformation.generated.h"

/**
 * 
 */
UCLASS()
class IRONGATE_API UServerInformation : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Player")
		static void CountPlayer(int player);

	UFUNCTION(BlueprintCallable, Category = "Status")
		static void GameStatus(bool status);

	UFUNCTION(BlueprintCallable, Category = "Start")
		static void StartGame();

	UFUNCTION(BlueprintCallable, Category = "Stop")
		static void StopGame();

	UFUNCTION(BlueprintCallable, Category = "Send")
		static void GameId(int id);

	UFUNCTION(BlueprintCallable, Category = "GetIP")
		static void ReceiveIP(FString ip);

	UFUNCTION(BlueprintCallable, Category = "FileUpload")
		static void UploadFile();

	static void CreateJsonFile();

	static long int GetLocalTime();
	
};
